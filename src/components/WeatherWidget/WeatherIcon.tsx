import React from 'react';
import {
  WiThunderstorm,
  WiRainMix,
  WiRain,
  WiSnow,
  WiSmoke,
  WiDaySunny,
  WiCloudy
} from 'react-icons/wi';

export const WeatherIcon = ({ weatherCode }: { weatherCode: number }): React.ReactElement => {
  if (weatherCode === undefined) return <p className='text-8xl'>...</p>;
  const [weatherId] = weatherCode.toString().split('');
  const code = parseInt(weatherId, 10); // 8
  let icon = null;
  switch (code) {
    case 0:
      icon = <WiCloudy />;
      break;
    case 2:
      icon = <WiThunderstorm />;
      break;
    case 4:
      icon = <WiRainMix />;
      break;
    case 5:
      icon = <WiRain />;
      break;
    case 6:
      icon = <WiSnow />;
      break;
    case 7:
      icon = <WiSmoke />;
      break;
    default:
      icon = <WiDaySunny />;
      break;
  }

  return icon;
};
