import React from 'react';

import { WeatherIcon } from './WeatherIcon';

import { CurrentProps } from '../../types';

import { toCelcius } from '../../utils';

export const Today = ({
  current,
}: {
  current: CurrentProps
}): React.ReactElement => {
  return (
    <div className='
      flex flex-col gap-4 items-center justify-center
      col-span-4 border-4 border-white rounded-t-xl'
    >
      <p className='text-2xl font-extralight tracking-wide'>Today</p>
      <div className='flex items-center justify-center gap-8'>
        <p className='text-8xl'>
          <WeatherIcon weatherCode={current?.weather?.[0].id} />
        </p>
        <div className='flex flex-col gap-4'>
          <p className='text-4xl md:text-6xl font-bold'>{toCelcius(current?.temp)}&deg;C</p>
          <p className='text-lg md:text-2xl font-extralight tracking-wide capitalize'>
            {current?.weather?.[0].description}
          </p>
        </div>
      </div>
    </div>
  );
};
