/* jshint sub:true */
import React from 'react';

import { WeatherIcon } from './WeatherIcon';

import { toCelcius, formatDate } from '../../utils';

export const WeekDay = ({
  weather
}: {
  weather: any
}): React.ReactElement => (
  <div className='flex flex-col items-center justify-between'>
    <p className='text-lg md:text-2xl font-extralight tracking-wide'>{formatDate(weather?.dt)}</p>
    <p className='text-4xl md:text-8xl'>
      <WeatherIcon weatherCode={weather?.weather?.[0].id} />
    </p>
    <p className='text-lg md:text-2xl font-bold'>{toCelcius(weather?.temp?.day)}&deg;C</p>
  </div>
);
