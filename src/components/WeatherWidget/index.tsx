import React from 'react';
import { Today } from './Today';
import { WeekDays } from './WeekDays';

export const WeatherWidget = ({
  forecast,
  isLoading
}: {
  forecast: any;
  isLoading: boolean
}):React.ReactElement => (
  <div
    className='
      flex items-center justify-center w-3/4 h-1/2
      background-white rounded-xl shadow-2xl
    '
  >
    <div className='grid grid-cols-4 w-full h-full'>
      {isLoading ? (
        <div className='col-span-4 flex items-center justify-center'>
          <p className='text-xl md:text-4xl font-extralight'>Loading...</p>
        </div>
      ) : (
        <React.Fragment>
          <Today current={forecast.current} />
          <WeekDays daily={forecast.daily} />
        </React.Fragment>
      )}
    </div>
  </div>
);
