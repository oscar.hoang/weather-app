import React from 'react';
import { WeekDay } from './WeekDay';

export const WeekDays = ({
  daily
}: {
  daily: {
    [key: string]: any
  }
}): React.ReactElement => (
  (daily || []).map((weather: any, index: number) => {
    if (index === 0) return null;
    return (
      <div
        key={`day-${index + 1}`}
        className={`
        flex items-center justify-center
          border-b-4 border-l-4 border-white
          ${index === 4 && 'border-r-4'}
          ${index === 1 && 'rounded-bl-xl'}
          ${index === 4 && 'rounded-br-xl'}
        `}
      >
        <WeekDay weather={weather} />
      </div>
    );
  })
);
