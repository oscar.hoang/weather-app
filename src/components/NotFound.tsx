import React from 'react';

export const NotFound: React.FC = () => (
  <div>Page Not Found</div>
);
