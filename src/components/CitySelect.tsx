import React from 'react';

import { CityProps } from '../types';

export const CitySelect = ({
  cities,
  selected,
  handleSelected
}: {
  cities: CityProps[],
  selected: any,
  handleSelected: (city: CityProps) => void;
}): React.ReactElement => (
  <div className='w-1/2 my-12'>
    <div className='grid sm:grid-cols-1 md:grid-cols-3 gap-4 w-full h-full'>
      {cities.map((city) => (
        <div
          key={city.name}
          className={`flex items-center justify-center`}
          onClick={() => handleSelected(city)}
        >
          <p
            className={`
              text-3xl font-extralight tracking-wide whitespace-nowrap
              uppercase text-center cursor-pointer hover:text-red-400
              ${city.id === selected.id && 'font-bold text-blue-400 hover:text-blue-400'}
            `}>
              {city.name}
            </p>
        </div>
      ))}
    </div>
  </div>
);
