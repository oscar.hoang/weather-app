export const toCelcius = (num: number) => Math.round(num - 273.15);

export const formatDate = (time: number) => {
  const date = new Date(time * 1000);
  return date.toString().split(' ')[0];
};
