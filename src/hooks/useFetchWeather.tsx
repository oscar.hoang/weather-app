import { useState, useEffect } from 'react';
import { CityProps } from '../types';

const API_KEY = 'b4be6dc74f77e2dafefed778b34e13e4';
const API_VERSION = '2.5';
const API_HOST = 'https://api.openweathermap.org';

const weatherApiUrl = ({ location: { lat, lon } }: { location: { lat: number, lon: number } }) => `
  ${API_HOST}/data/${API_VERSION}/onecall?lat=${lat}&lon=${lon}&appid=${API_KEY}&exclude=minutely,hourly,alerts
`;

export const useFetchWeather = (city: CityProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState({});
  const [forecast, setForecast] = useState({});
  useEffect(() => {
    if (city) {
      (async () => {
        try {
          setIsLoading(true);
          const res = await fetch(weatherApiUrl(city));
          const data = await res.json();
          data.daily.splice(5, 3);
          setForecast(data);
        } catch (err) {
          if (err instanceof Error) {
            setError(err);
          }
        }
        setIsLoading(false);
      })();
    }
  }, [city]);

  return { forecast, isLoading, error };
};
