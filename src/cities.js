export default [{
  id: 1,
  name: 'Moscow',
  location: {
    lat: 37.6173,
    lon: 55.7558
  }
}, {
  id: 2,
  name: 'New York',
  location: {
    lat: 74.0060,
    lon: 40.7128
  }
}, {
  id: 3,
  name: 'Ottawa',
  location: {
    lat: 75.6972,
    lon: 45.4215
  }
}];
