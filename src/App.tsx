import React, { useState } from 'react';

import { CitySelect } from './components/CitySelect';
import { WeatherWidget } from './components/WeatherWidget';

import { useFetchWeather } from './hooks/useFetchWeather';

import { CityProps } from './types';

import data from './cities';

const App: React.FC = () => {
  const [selected, setSelected] = useState<CityProps>(data[0]);

  const { forecast, isLoading } = useFetchWeather(selected);

  const handleSelected = (location: CityProps) => {
    setSelected(location);
  };

  return (
    <div className='container mx-auto flex flex-col items-center justify-center h-screen'>
      <CitySelect
        cities={data}
        selected={selected}
        handleSelected={handleSelected} />
      <WeatherWidget
        forecast={forecast}
        isLoading={isLoading}
      />
    </div>
  );
};

export default App;
