export interface CityLocationProps {
  lon: number;
  lat: number;
}
export interface CityProps {
  id: number;
  name: string;
  location: CityLocationProps;
}

export interface WeatherProps {
  description: string;
  icon: string;
  id: number;
  main: string;
}

export interface CurrentProps {
  clouds: number;
  dew_point: number;
  dt: number;
  feels_like: number;
  humidity: number;
  pressure: number;
  sunrise: number;
  sunset: number;
  temp: number;
  uvi: number;
  visibility: number;
  weather: WeatherProps[];
  wind_deg: number;
  wind_gust: number;
  wind_speed: number;
}
